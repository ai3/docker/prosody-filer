FROM golang:1.17 AS build

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go install -ldflags="-w -s" github.com/ThomasLeister/prosody-filer@v1.0.2

FROM scratch
COPY --from=build /go/bin/prosody-filer /prosody-filer

ENTRYPOINT ["/prosody-filer"]
